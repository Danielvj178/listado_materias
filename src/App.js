import React, { Component } from 'react';
import Header from './components/Header';
import Footer from './components/Footer';
import Materias from './components/Materias';

class App extends Component {
  render() {
    const year = new Date().getFullYear();
    const materias = [
      {nombre:"BD", nota:5},
      {nombre:"Cálculo", nota:1},
      {nombre:"Ces4", nota:4},
      {nombre:"Matemáticas", nota:3},
    ];
    return (
      /** Con JSX */
      /*<div id="titulo" className="container"> 
        <Header/>
        <Footer/>
      </div>*/

      /** Envio de propiedades del padre al hijo */
      <React.Fragment>
        <Header 
          titulo="My APP React"
          mensaje="Vamos la banda"
          materias={materias}
          color="green"
          fontSize="20px"
        />
        <Materias 
          materias={materias}
        />
        <Footer
          year={year}
          nombre="Daniel"
        />
      </React.Fragment>

      /** Sin JSX */
      /*React.createElement('div', {id:"container",},
      React.createElement('h1', {id:"letras"}, "Hola mundo"))*/
    );
  }
}

export default App;

import React from 'react'

export default function Materia({ i, materia }) {
  return (
    <React.Fragment>
      <li key={i}>{`Materia: ${materia.nombre} - Nota: ${materia.nota}`}</li>
    </React.Fragment>
  )
}

import React, { Component } from 'react';

export default class Header extends Component{
    //Recibir propiedades enviadas por el padre
    render(){
        const {titulo, mensaje, color, fontSize} = this.props;
        const style = {
            color,
            fontSize,
        };

        return(
            <header style={style}>
                <h3>{`${titulo} - ${mensaje}`}</h3>
                <h4>{mensaje}</h4>
            </header>
        );
    }
}
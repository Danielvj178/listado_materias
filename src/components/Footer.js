//rcf snippet

import React from 'react';
/** En los componentes funcionales no existe this, si se envían propiedades se deben recibir como parametro */
export default function Footer({year, nombre}) {
  return (
    <div>
        <footer>
          By {nombre} Vidal &copy; {year} 
        </footer>
    </div>
  )
}
